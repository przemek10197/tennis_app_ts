import { TennisPlayer } from '@/modules/playerModule/models/tennis-player-model';

export type TournamentsList = Array<Tournament>;

export interface Tournament {
  name: string;
  dateStart: Date | any;
  dateEnd: Date | any;
  place: string;
  prizePool?: number;
  players?: Array<TennisPlayer>;
}
