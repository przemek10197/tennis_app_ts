import { TournamentsList, Tournament } from '@/modules/tournamentModule/models/tournaments-model';
import TournamentsModuleStore from '@/modules/tournamentModule/store/tournaments.store';

export class TournamentsModuleStoreFacade {

  public addTournament(tournament: Tournament) {
    TournamentsModuleStore.addTournament(tournament);
  }

  public removeTournament(tournamentName: string) {
    TournamentsModuleStore.removeTournament(tournamentName);
  }

  public get tournaments(): TournamentsList {
    return TournamentsModuleStore.tournamentsList;
  }
  
}

export default new TournamentsModuleStoreFacade;