import { Action, getModule, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import Store from '@/store';
import { TournamentsList, Tournament } from '@/modules/tournamentModule/models/tournaments-model';

export enum TournamentsModuleActions {
  addTournament = "extendTournament",
  removeTournament = "deleteTournament"
}

@Module({ dynamic: true, store: Store, name: 'TournamentsStoreModule', namespaced: true })
class TournamentsModuleStore extends VuexModule {
  public tournamentsList: TournamentsList = [
    {
      name: "Australian Open 2020",
      dateStart: new Date("2020-01-26"),
      dateEnd: new Date("2020-01-31"),
      place: "Melbourne",
      prizePool: 1000000
    }
  ];

  @Action({commit: TournamentsModuleActions.addTournament})
  public addTournament(tournament: Tournament): Tournament {
    return tournament;
  }

  @Action({commit: TournamentsModuleActions.removeTournament})
  public removeTournament(tournamentName: string): string {
    return tournamentName;
  }

  @Mutation
  private extendTournament(tournament: Tournament): void {
    this.tournamentsList.push(tournament);
  }

  @Mutation
  private deleteTournament(tournamentName: string): void {
    for(let i = 0; i < this.tournamentsList.length; i++) {
      if (this.tournamentsList[i].name === tournamentName) {
        this.tournamentsList.splice(i, 1);
      }
    }
  }
}

export default getModule(TournamentsModuleStore);