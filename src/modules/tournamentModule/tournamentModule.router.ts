export const tournamentModuleRoutes = [
  {
    path: '/tournaments',
    component: () => import ('@/modules/tournamentModule/components/tournaments.component.vue'),
    children: [
      {
        path: '',
        component: () => import('@/modules/layoutModule/components/navbar.component.vue')
      }
    ]
  }
]
