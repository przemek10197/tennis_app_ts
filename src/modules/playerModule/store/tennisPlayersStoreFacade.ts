import { TennisPlayersList, TennisPlayer } from "@/modules/playerModule/models/tennis-player-model";
import TennisPlayersModuleStore from '@/modules/playerModule/store/tennisPlayers.store';

export class TennisPlayersModuleStoreFacade {

  public addPlayer(player: TennisPlayer) {
    TennisPlayersModuleStore.addPlayer(player);
  }

  public removePlayer(name: string) {
    TennisPlayersModuleStore.removePlayer(name);
  }

  public loadPlayers() {
    TennisPlayersModuleStore.loadPlayers();
  }

  public get players(): TennisPlayersList {
    return TennisPlayersModuleStore.tennisPlayersList;
  }
}

export default new TennisPlayersModuleStoreFacade;