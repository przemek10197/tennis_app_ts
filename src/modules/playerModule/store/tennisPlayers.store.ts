import {Action, getModule, Module, Mutation, VuexModule} from 'vuex-module-decorators';
import Store from '@/store';
import {TennisPlayersList, TennisPlayer} from '@/modules/playerModule/models/tennis-player-model';
import playersImported from '@/assets/atp-rankings.json';

export enum TennisPlayersModuleActions {
  addPlayer = 'extendPlayer',
  removePlayer = 'deletePlayer',
  loadPlayers = 'setPlayers'
}

function compareByRank(a, b) {
  let comparison = 0;
  comparison = (a.rank > b.rank) ? 1 : -1;
  return comparison;
}

@Module({ dynamic: true, store: Store, name: 'TennisPlayersModuleStore', namespaced: true })
class TennisPlayersModuleStore extends VuexModule {
  public tennisPlayersList: TennisPlayersList = [];

  @Action({commit: TennisPlayersModuleActions.addPlayer})
  public addPlayer(player: TennisPlayer): TennisPlayer {
    return player;
  }

  @Action({commit: TennisPlayersModuleActions.removePlayer})
  public removePlayer(name: string) {
    return name;
  }

  @Action({ commit: TennisPlayersModuleActions.loadPlayers })
  public loadPlayers() {
    return playersImported.slice(0, 30);
  }

  @Mutation
  private extendPlayer(player: TennisPlayer): void {
    this.tennisPlayersList.push(player);
  }

  @Mutation 
  private deletePlayer(playerName: string): void {
    for(let i = 0; i < this.tennisPlayersList.length; i++) {
      if (this.tennisPlayersList[i].name === playerName)
        this.tennisPlayersList.splice(i, 1);
    }
  }

  @Mutation
  private setPlayers(players) {
    for(const player of players) {
      const playerToAdd = {
        name: player.fields.player_name,
        country: player.fields.player_country,
        rank: player.fields.current_rank,
        points: player.fields.player_points
      }
      this.tennisPlayersList.push(playerToAdd);
    }
    this.tennisPlayersList.sort(compareByRank);
  }

  public get players() {
    return this.tennisPlayersList;
  }
}

export default getModule(TennisPlayersModuleStore);