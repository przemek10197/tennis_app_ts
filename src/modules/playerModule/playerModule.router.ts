export const playerModuleRoutes = [
  {
    path: '/',
    component: () => import ('@/modules/playerModule/components/players.component.vue'),
    children: [
      {
        path: '',
        component: () => import('@/modules/layoutModule/components/navbar.component.vue')
      }
    ]
  }
]