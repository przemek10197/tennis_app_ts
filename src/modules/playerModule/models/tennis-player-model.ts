export type TennisPlayersList = Array<TennisPlayer>

export interface TennisPlayer {
  name: string;
  country: string;
  rank: number | null;
  points?: number | null;
}