import Vue from 'vue';
import VueRouter from 'vue-router';
import {playerModuleRoutes} from '@/modules/playerModule/playerModule.router';
import {tournamentModuleRoutes} from '@/modules/tournamentModule/tournamentModule.router';

Vue.use(VueRouter);

const route = {
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		...playerModuleRoutes,
		...tournamentModuleRoutes
	]
}

// @ts-ignore
export default new VueRouter(route);




// const routes = [
// 	...playerModuleRoutes,
// 	...tournamentModuleRoutes
// ]

// const router = new VueRouter({
// 	mode: 'history',
// 	base: process.env.BASE_URL,
// 	routes
// });
